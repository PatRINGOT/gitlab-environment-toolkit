include:
  - template: Security/Dependency-Scanning.gitlab-ci.yml

stages:
  - test

#################################
##     Dependency Scanning     ##
#################################

.on_merge_request_and_merged_events:
  rules:
    - if: '$CI_BUILD_REF_NAME == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE == "push" || $CI_PIPELINE_SOURCE == "merge_request_event"'

gemnasium-dependency_scanning:
  rules:
    - !reference [.on_merge_request_and_merged_events, rules]

gemnasium-python-dependency_scanning:
  rules:
    - !reference [.on_merge_request_and_merged_events, rules]

bundler-audit-dependency_scanning:
  rules:
    - !reference [.on_merge_request_and_merged_events, rules]


#########################
##     Lint Checks     ##
#########################

check:terraform-fmt:
  stage: test
  image:
    name: "hashicorp/terraform"
    entrypoint: [ "" ]
  before_script:
    - terraform version
  script:
    - terraform fmt -check -recursive -diff terraform/
  only:
    - main
    - merge_requests
  except:
    - schedules
    - tags

check:tflint:
  stage: test
  image:
    name: ghcr.io/terraform-linters/tflint:latest
    entrypoint: [""]
  script:
    - find terraform/modules/* -type d -print0 | xargs -0 -n1 -I % sh -c "printf '\n%\n' && tflint -c .tflint.hcl %"
  only:
    - main
    - merge_requests
  except:
    - schedules
    - tags

check:ansible-lint:
  stage: test
  image: quay.io/ansible/toolset
  script:
    - chmod 700 ansible
    - cd ansible
    - ansible-lint
  only:
    - main
    - merge_requests
  except:
    - schedules
    - tags

check:markdownlint:
  stage: test
  image:
    name: node:lts-alpine3.14
    entrypoint: [ "" ]
  script:
    - npm add -g markdownlint markdownlint-cli
    - markdownlint *.md docs/*.md
  only:
    - main
    - merge_requests
  except:
    - schedules
    - tags

check:markdown-link-check:
  stage: test
  image:
    name: ghcr.io/tcort/markdown-link-check
    entrypoint: [""]
  script:
    - find *.md docs/*.md -print0 | xargs -0 -n1 /src/markdown-link-check
  only:
    - main
    - merge_requests
  except:
    - schedules
    - tags

# Does the requirements.txt install?
check_requirements:
  stage: test
  image: python:3
  script:
    - python -m venv get-python-env
    - source ./get-python-env/bin/activate
    - pip install -r ansible/requirements/requirements.txt
    - ansible-galaxy install -r ansible/requirements/ansible-galaxy-requirements.yml
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes:
        - ansible/requirements/requirements.txt
        - ansible/requirements/ansible-galaxy-requirements.yml
